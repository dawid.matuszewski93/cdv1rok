using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyFirstScript : MonoBehaviour
{
    public string MyName;
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log(MyName);
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("Hello Again!");
    }   
}
